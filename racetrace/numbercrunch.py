from pandas import isna
import numpy as np


# meaing of documented fastf1 trackstatus codes
fastf1_trackstatus_meaning = {
    '1'     : 'Track clear',
    '2'     : 'Yellow flag (sectors unknown)',
    '3'     : '???',
    '4'     : 'Safety Car',
    '5'     : 'Red Flag',
    '6'     : 'Virtual Safety Car deployed',
    '7'     : 'Virtual Safety Car ending',
    '99999' : 'Mixed trackstatus'
    }

#------------------------------------------------------------------------------


def get_trackstatus_asnumber(winner_laptimes, winner_index_offset, winner_nlaps):
    trackstatus_int = np.ones(winner_nlaps, dtype=np.int32)

    for i in range(1,winner_nlaps):
        # there have been cases where TrackStatus is Nan, we just consider it "not 1"
        try:
            trackstatus_int[i] = int(winner_laptimes['TrackStatus'][winner_index_offset + i])
        except:
            trackstatus_int[i] = 99999

        if trackstatus_int[i] > 7:
            trackstatus_int[i] = 99999

    return trackstatus_int



def compute_offset_of_traces(winner_laptimes, winner_index_offset, winner_nlaps, trackstatus_int):
    # initialize array of correct type
    offset_of_traces = [winner_laptimes['Time'][winner_index_offset]]*winner_nlaps

    average_winner_lt = compute_average_winner_laptime(winner_laptimes, winner_index_offset, winner_nlaps, trackstatus_int)

    for i in range(1, winner_nlaps):
        # for every racing lap we skew all the traces with the average winner pace
        # (which has just been computed on the same racing laps)
        if trackstatus_int[i] == 1:
            if isna( winner_laptimes['LapTime'][winner_index_offset + i] ) :
                #if there are NaT values we skew all the traces with the exact laptime of the winner
                offset_of_traces[i] = ( offset_of_traces[i-1] 
                                + winner_laptimes['Time'][winner_index_offset + i] 
                                - winner_laptimes['Time'][winner_index_offset + i -1] )

                print(i, 'Offset some non-racing lap based on NaT values. Trackstatus : ', fastf1_trackstatus_meaning[str(trackstatus_int[i])])
            else:
                offset_of_traces[i] = offset_of_traces[i-1] + average_winner_lt

        else:
            # for every non-racing lap we skew all the traces with the exact laptime of the winner
            offset_of_traces[i] = ( offset_of_traces[i-1] 
                                + winner_laptimes['Time'][winner_index_offset + i] 
                                - winner_laptimes['Time'][winner_index_offset + i -1] )

            print(i, 'Offset some non-racing lap. Trackstatus : ', fastf1_trackstatus_meaning[str(trackstatus_int[i])])

    return offset_of_traces



def compute_average_winner_laptime(winner_laptimes, winner_index_offset, winner_nlaps, trackstatus_int):
    # initialize average winner laptimes with right type (laptime) but value zero (difference between same value = 0)
    average_winner_lt = ( winner_laptimes['Time'][winner_index_offset] 
                        - winner_laptimes['Time'][winner_index_offset] )

    racing_laps = 0

    # compute correct average winner laptime only on racing laps
    for i in range(1,winner_nlaps):
        # we compute the average winner pace from his racing laps
        if trackstatus_int[i] == 1 and not isna( winner_laptimes['LapTime'][winner_index_offset + i] ):
            racing_laps   += 1
            average_winner_lt += ( winner_laptimes['Time'][winner_index_offset + i] 
                                    - winner_laptimes['Time'][winner_index_offset + i-1] )

    average_winner_lt = average_winner_lt / racing_laps
    return average_winner_lt




def adjust_lap_number(from_lap, to_lap, winner_nlaps):
    if to_lap < winner_nlaps:
        nlaps = to_lap
    else:
        nlaps = winner_nlaps
    if from_lap > 0:
        nlaps -= from_lap

    return nlaps

