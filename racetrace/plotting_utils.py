import matplotlib.pyplot as plt
import fastf1.plotting

import previous_years_dicts
import numbercrunch


def make_plot(session, from_lap, to_lap, winner_nlaps, offset_of_traces, year, trackstatus_int):
    # setup matplolib environment variables
    fastf1.plotting.setup_mpl()
    fig, ax = plt.subplots()

    plot_driver_traces(ax, session, from_lap, to_lap, winner_nlaps, offset_of_traces, year, trackstatus_int)

    fig.legend()
    ax.grid()
    fig.show()



def plot_driver_traces(ax, session, from_lap, to_lap, winner_nlaps, offset_of_traces, year, trackstatus_int):
    driver_colors, driver_translate = get_fastf1_colors(year)

    for d in session.drivers:
        laptimes     = session.laps.pick_driver(d)
        
        # easier to ignore empty driver dataframes for now
        if len(laptimes) == 0:
            continue

        drivername   = laptimes['Driver'][laptimes.index[0]]
        drivernumber = laptimes['DriverNumber'][laptimes.index[0]]
        teamname     = laptimes['Team'][laptimes.index[0]]


        current_nlaps = numbercrunch.adjust_lap_number(from_lap, to_lap, len(laptimes) )

        driver_trace = compute_trace_of_driver(current_nlaps, laptimes, offset_of_traces, from_lap, to_lap)

        ax.plot(laptimes['LapNumber'][:len(driver_trace)], driver_trace ,
                color=driver_colors[driver_translate[drivername]],
                label=drivername)

    must_adjust_ylim(ax)
    draw_anomalous_trackstatus(ax, winner_nlaps, trackstatus_int)






def compute_trace_of_driver(current_nlaps, laptimes, offset_of_traces, from_lap, to_lap):
    driver_trace = [0] * current_nlaps
    for i in range(current_nlaps):
        driver_trace[i] = -(offset_of_traces[i+from_lap] - laptimes['Time'][(laptimes.index[0] + i + from_lap)])

    return driver_trace



def get_fastf1_colors(year):
    # for current year we can rely on the defined colors and driver dictionaries
    if year >= 2024:
        driver_colors    = fastf1.plotting.DRIVER_COLORS
        driver_translate = fastf1.plotting.DRIVER_TRANSLATE
    # we can't do that for previous years, use custom dicts
    else:
        driver_colors    = previous_years.dicts.driver_colors_dict[year]
        driver_translate = previous_years.dicts.driver_translate_dict[year]

    return driver_colors, driver_translate



def must_adjust_ylim(ax):
    current_min, current_max = ax.get_ylim()
    ax.set_ylim([current_max, current_min])


def draw_anomalous_trackstatus(ax, winner_nlaps, trackstatus_int):
    ax.fill_between(range(winner_nlaps), 1, -1, 
            where= trackstatus_int > 1, 
            color='yellow', alpha=0.2, transform=ax.get_xaxis_transform())

    print(trackstatus_int)
