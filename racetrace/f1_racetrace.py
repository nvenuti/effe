import fastf1

import sys
import numpy as np

from argparse import ArgumentParser

import plotting_utils
import numbercrunch


# Script to get 'race trace' plot of F1 sessions, using fastf1

# session.laps.pick_driver(winner)['TrackStatus'] gives information on which laps are
# real and which aren't
# mmaybe create traces to plot lap per lap? 
# So everytime you can choose how to skew traces

# done in theory, needs testing

# NB THINK ABOUT COMPATIBILITY WITH from/to_lap OPTIONS



#------------------------------------------------------------------------------

def initialize_parser():
    parser = ArgumentParser()
    parser.add_argument('--year',     type=int, help='From what year',              required=True)
    parser.add_argument('--event',    type=str, help='Typical name of the GP',      required=True)
    parser.add_argument('--session',  type=str, help='Default is \'Race\' ',        required=False, default='Race')
    parser.add_argument('--from_lap', type=int, help='Ignore laps up to this one',  required=False, default=0)
    parser.add_argument('--to_lap',   type=int, help='Ignore laps after this one',  required=False, default=1000)

    return parser




def niceprompt():
    # isolate nicely the library output from my custom one
    print('''

End of fastf1 std output log.
------------------------------------------------------------------------------

    ''')



#------------------------------------------------------------------------------

def main():

    parser = initialize_parser();
    year     = parser.parse_args().year
    gp_name  = parser.parse_args().event
    from_lap = parser.parse_args().from_lap
    to_lap   = parser.parse_args().to_lap
    session  = parser.parse_args().session

    session = fastf1.get_session(year, gp_name, session)
    session.load()

    niceprompt()

    winner_drivernumber = session.results['DriverNumber'][0]
    winner_laptimes     = session.laps.pick_driver(winner_drivernumber)
    winner_nlaps        = len(winner_laptimes['LapNumber'])


    # the "index" in fastf1 dataframes spans all drivers
    # so the first index is different for everybody
    winner_index_offset = session.laps.pick_driver(winner_drivernumber).index[0]

    trackstatus_int = numbercrunch.get_trackstatus_asnumber(winner_laptimes, winner_index_offset, winner_nlaps)

    offset_of_traces = numbercrunch.compute_offset_of_traces(winner_laptimes, winner_index_offset, winner_nlaps, trackstatus_int)

    plotting_utils.make_plot(session, from_lap, to_lap, winner_nlaps, offset_of_traces, year, trackstatus_int)

    input('Enter when done...')


#------------------------------------------------------------------------------


try:
    main()
except:
    import traceback, pdb
    traceback.print_exc()
    pdb.post_mortem()
    sys.exit(1)
