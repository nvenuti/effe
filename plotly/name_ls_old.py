#LISTE DI TUTTI I NOMI E COLORI

#MAGARI FACCI DEI DIZIONARI....

#PILOTI IN ORDINE CRESCENTE DI "ABILITà"
#COLORI CORRISPONDENTI

names_21 = ['Mazpain', 'Schiumako', 'Lotif', 'Giovizio', 'Rassei', 'Reikkone',
         'Zunida', 'Straul', 'Occam', 'Reckardo', 'Alenzo', 'Gasny', 'Vetil',
         'Senz', 'Peras', 'Marriss', 'Leclair', 'Butos', 'Verstomp', 'Hammerton']

#SI PUò USARE SOLO CON matplotlib
'''
colors = ['pink', 'tab:pink', 'blue', 'tab:brown', 'darkblue', 'brown', 
          'blueviolet', 'green', 'slategrey', 'silver', 'orange', 'navy', 'tab:green',
          'red', 'tab:purple', 'tab:orange', 'crimson', 'tab:cyan', 'darkmagenta', 'cyan']
'''
#USA QUESTI COLORI QUI
css_colors_21 = ['pink', 'palevioletred', 'blue', 'saddlebrown', 'dodgerblue', 'brown', 
          'blueviolet', 'green', 'slategrey', 'orange', 'silver', 'navy', 'seagreen',
          'red', 'mediumpurple', 'darkorange', 'crimson', 'mediumturquoise', 'darkmagenta', 'cyan']


names_22 = ['Lotif', 'Straul', 'Reckardo', 'Albawn', 'Gio', 'Schiumako',
     	'Zunida', 'Vetil', 'Occam', 'Marriss', 'Maagnuus', 'Alenzo', 'Gasny',
    	'Butos', 'Rassei', 'Hammerton', 'Senz', 'Peras', 'Leclair', 'Verstomp']

css_colors_22 = ['blue', 'seagreen', 'orange', 'dodgerblue', 'saddlebrown', 'pink',  
          'blueviolet', 'green', 'slategrey', 'darkorange', 'palevioletred', 'silver', 'navy',
          'brown', 'mediumturquoise', 'cyan', 'red', 'mediumpurple', 'crimson', 'darkmagenta']


#ANCORA NON USATO
teams = ['Mercados', 'Red Cow', 'Firarro', 'Mclaar', 'Al Fatori',           
         'Asto Marte', 'Alpini', 'Al Faro Mio', 'Willy Ams', 'Hesse']

teams_col = ['cyan', 'darkmagenta', 'crimson', 'tab:orange', 'navy',
             'green', 'slategrey', 'brown', 'blue', 'tab:pink']
