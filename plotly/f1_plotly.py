# ISTRUZIONI CHE NON SERVE RIPETERE AD OGNI ESECUZIONE

import plotly.graph_objects as go
from plotly.subplots import make_subplots
#import plotly.express as px
import plotly.io as pio
pio.renderers.default = 'browser'
# pio.renderers.default = 'iframe'
# pio.renderers.default = "jupyterlab"
# pio.renderers.default = "notebook+jupyterlab+pdf"

import time
import numpy as np

#LISTE DI TUTTI I NOMI E COLORI
import name_ls

# VARIABILI GLOBALI

# seed = 221087
# rng = np.random.default_rng(seed)

races = 23  #uno  in più del valore effettivamente voluto
grid = 20
teams = 10

# ogni #reglength anni si genera una nuova gerarchia di base
# ripetuto #ncycles volte
ncycles = 12
reglength = 3
years = ncycles * reglength


# nomi sono ordinati secondo classifica reale squadre anno precedente
# (che è pure l'ordinamento delle squadre)

names = name_ls.names_21
colors = name_ls.css_colors_21
constr_names = name_ls.teams
constr_colors = name_ls.teams_col

#-----------------------------------------------------------------------------------
# funzioni che non serve modificare

def compute_points(num, wins):
    # per ogni gara, crea l'ordine di arrivo
    # a partire dai numeri casuali generati
    # accumula punti
    # e accumula informazioni sulle vittorie
    
    for i in range(1, races):
        #ordina i risultati delle gare per assegnare punteggi
        val = np.sort(num[:, i])
    
        for k in range(0, grid):
            #ora i risultati ordinati di ogni gara vengono trasformati in punti
            if num[k, i] < val[10]:
                num[k, i] = 0
            elif num[k, i] == val[10]:
                num[k, i] = 1
            elif num[k, i] == val[11]:
                num[k, i] = 2
            elif num[k, i] == val[12]:
                num[k, i] = 4
            elif num[k, i] == val[13]:
                num[k, i] = 6
            elif num[k, i] == val[14]:
                num[k, i] = 8
            elif num[k, i] == val[15]:
                num[k, i] = 10
            elif num[k, i] == val[16]:
                num[k, i] = 12
            elif num[k, i] == val[17]:
                num[k, i] = 15
            elif num[k, i] == val[18]:
                num[k, i] = 18
            elif num[k, i] == val[19]:
                num[k, i] = 25
                #registra informazione che pilota k-esimo ha vinto gara
                wins[i-1] = k
            else:
                print('errore, non so come classificare questo risultato', k, i)
    
    return num, wins

# ---------------------------------------------------------------------------------------
# flusso della simulazione:
#     -per ogni stagione
#         -si parte dalle gerarchie base per:
#             -piloti
#             -squadre
#         -indipendentemente vengono generate le prestazioni casuali ad ogni gara
#         -inoltre vengono generati i coefficienti casuali di sviluppo per
#             -piloti
#             -squadre
#         -si unisce il tutto per avere il risultato complessivo
#     -ogni tot. stagioni la gerarchia di base viene cambiata
    
# -come modificare le gerarchie in maniera realistica?
# -quanto peso dare a prestazioni pilota vs squadra?

# ---------------------------------------------------------------------------------------
# Osservazioni:
# 1)
    # le annate dove le squadre sono molto vicine si nota molto di più la variabilità
    # forse bisogna modificare come viene espressa la variabilità per avere effetti più costanti?
    
# TODO : introdurre propensione alle avarie diversa per ogni squadra


start = time.time()
# coefficienti (ANCHE QUESTE VARIABILI GLOBALI)

# variabilità prestazioni per singole gare
race_var = 0.22

# importanza relativa piloti/squadre
# nelle prestazioni
pt_coeff = 1
# nella gerarchia
l_pt_coeff = 0.07

# importanza relativa prestazioni/gerarchia
pl_coeff = 0.8

# scala dell'evoluzione della gerarchia tra singoli anni
yr_diff = 0.1
# probabilità dell'evoluzione di una squadra tra i singoli anni
evo_prob = 0.6

#probabilità di dnf di ognuno ad ogni gara
dnf_prob = 0.084


# costanti della distribuzione di base gerarchia
# altitudine del minimo della curva
mn = 13
# posizione in x del gradino
offset = 0.79
# pendenza del gradino
slope = 3

# distribuzione di base gerarchia
# viene usata *tanh* con varie costanti per avere forma desiderata:
# permette di regolare competitività delle varie zone della classifica
master_levels = (np.tanh(slope*np.pi*(np.linspace(0.1, 1, teams)-offset))+1+mn) /(2+mn)


#-----------------------------------------------------------------------------------------
# funzioni



def create_levels(levels_eff):
    # genera una gerarchia piloti (indici 1 e 2, 10+10 piloti) e squadre (indice 0)
    # in maniera casuale

    team_lev = np.random.choice( master_levels , size=teams, replace=False)
    levels_eff[:teams] = team_lev + l_pt_coeff * np.random.random()
    levels_eff[teams:] = team_lev + l_pt_coeff * np.random.random()
    return levels_eff



def update_levels(levels_eff):
    # genera una nuova gerarchia piloti (indici 1 e 2, 10+10 piloti) e squadre (indice 0) 
    # adattando stato dell'anno precedente alla curva di riferimento
    # essenzialmente rende permanenti le eventuali modifiche causate dall'evoluzione marginale

    team_lev = master_levels
    
    t_lev = np.zeros(teams)
    for i in range(teams):
        t_lev[i] = levels_eff[i] + levels_eff[i+teams]
        
    # registra la gerarchia delle squadre raggiunta
    indices = np.argsort(t_lev)
    
    for i in range(teams):
        # assegna nuovi master_levels (+ fattore random) alle coppie di piloti
        # secondo la gerarchia attuale della loro squadra
        ind = indices[i]
        if ind < teams:
            levels_eff[ind]       = team_lev[i] + l_pt_coeff * np.random.random()
            levels_eff[ind+teams] = team_lev[i] + l_pt_coeff * np.random.random()
        else:
            levels_eff[ind]       = team_lev[i] + l_pt_coeff * np.random.random()
            levels_eff[ind-teams] = team_lev[i] + l_pt_coeff * np.random.random()
    return levels_eff


    
def marginal_evo(levels_eff):
    # evoluzione marginale della gerarchia ogni anno
        
    # costruzione di una soglia di levels aggiustabile
    # sotto alla quale far agire l'evoluzione annuale
    # (per rendere le cose più competitive,
    # evolvono tutti tranne i piloti con level piu'
    # alto della soglia)
    high = np.amax(levels_eff)
    low = np.min(levels_eff)
    pos = 0.95
    trigger = (high-low) * pos + low

    move = yr_diff * np.random.random(teams)

    for i in range(teams):

        if np.random.random() < evo_prob:
            if levels_eff[i] < trigger:
                levels_eff[i] =  levels_eff[i] + move[i]
            if levels_eff[i+teams] < trigger:
                levels_eff[i+teams] =  levels_eff[i+teams] + move[i]
                
    return levels_eff
    
    
def dnf_insert(num, yr_dnf, dnf_count):
        # dnf casuali molto semplici che combinano elemento di squadra stagionale e individuale
        # NB non stiamo controllando che NON ci siano 10+ avarie nella stessa gara 
        # (che sarebbe un problema per la funzione compute_points)
        failcheck_teams = np.random.random(teams)*0.7 + 0.3
        failcheck_pilots = np.random.random((grid, races))
        for i in range(races):
            failcheck_pilots[:10, i] = 1.2*failcheck_pilots[:10, i]*failcheck_teams[:]
            failcheck_pilots[10:, i] = 1.2*failcheck_pilots[10:, i]*failcheck_teams[:]

        for i in range(grid):
            for p in range(races):
                if failcheck_pilots[i, p] < dnf_prob:
                    # conta quanti dnf bisognerà plottare
                    dnf_count += 1
                    num[i, p] = 0
                    # salvi gara e pilota per cui e' avvenuto il dnf
                    yr_dnf[dnf_count] = [p, i]
                    
        return num, yr_dnf, dnf_count
    
    

#----------------------------------------------------------------------------------------------------------------------------------
def main():
    
    # gerarchia effettiva
    levels_eff = np.zeros((years, grid))

    # risultati finali delle stagioni
    champ_p= np.zeros((years, grid))
    champ_t= np.zeros((years, grid))

    # andamento del punteggio cumulativo ogni stagione
    pilots = np.zeros((years, grid, races))

    # prestazioni ad ogni gara
    perf_eff = np.zeros((years, grid, races))

    #risultati grezzi complessivi ogni gara
    results = np.zeros((years, grid, races))

    # dove immagazziniamo informazioni per mettere etichette 'W'
    wins = np.zeros((years, races))
    # dove immagazziniamo informazioni per mettere etichette 'W' per le squadre
    t_wins = np.zeros((years, races))
    # dove immagazziniamo informazioni per indicare dnf
    # creiamo all'inizio una lista +lunga del necessario
    # safe = valore di aspettazione + 5 sigma
    safe = int(dnf_prob*races*grid + 5 * (dnf_prob*(1-dnf_prob)*races*grid))
    provv_dnf = np.zeros((years, safe, 2), dtype=np.int32)
    dnf_count = np.zeros((years), dtype=np.int32)
    dnf_markers = [0]*years

    
    # posizione dove scrivere etichetta 'Champ.'
    champ_check = np.zeros((years, 2))
    
    yearplots_titles = [0]*2*years

    for l in range(0, years):

        # yearplots_titles[2*l]   = 'WDC, year '+str(l+1)
        # yearplots_titles[2*l+1] = 'WCC, year '+str(l+1)
        
        if l == 0:
            # crea nuova gerarchia
            levels_eff[l] = create_levels(levels_eff[l])
        elif l > 0 and l % reglength == 0:
            # modifica della gerarchia all'inizio di un nuovo ciclo
            levels_eff[l] = update_levels(levels_eff[l-1])
        elif l > 0 and l % reglength != 0:
            # evoluzione marginale della gerarchia ogni anno
            levels_eff[l] = marginal_evo(levels_eff[l-1])
        
        # fix per evitare che per un anno ci sia un unico
        # pilota molto avanti rispetto agli altri
        for i in range(grid):
            if levels_eff[l, i] > 1.1:
                levels_eff[l, i] = 1.1
                
        
        # prestazioni relative di ogni pilota (indici 1 e 2, 10+10 piloti) e squadra (indice 0) ad ogni gara 
        # +1 è valore di fondo su cui agisce fattore casuale
        perf_array = (1 - race_var * np.random.random(size=(3, teams, races))) /2

        perf_eff[l, :teams] = perf_array[0] + pt_coeff * perf_array[1] 
        perf_eff[l, teams:] = perf_array[0] + pt_coeff * perf_array[2]


        # genera i risultati effettivi delle gare unendo gerarchia e prestaioni
        # NB prima colonna viene lasciata ==0
        # NB capire modi migliori di mischiare gerarchia e prestazioni
        for i in range(grid):
            results[l, i] = levels_eff[l, i] + perf_eff[l, i] * pl_coeff * (1 - 0.3*(l%reglength)/reglength)
                                                #nb spiegare perchè riduci la casualità da prestazioni verso la fine della reglength

                    
        results[l], provv_dnf[l], dnf_count[l] = dnf_insert(results[l], provv_dnf[l], dnf_count[l])
        
        dnf_markers[l] = np.zeros((dnf_count[l], 2))
#         print(dnf_count[l])
                
        results[l], wins[l] = compute_points(results[l], wins[l])


        # puliamo array dove immaganizziamo andamento della stagione
        pilots[l, :, :] = 0
        
        
        for i in range(1, races):
            for k in range(0, grid):
                # andamento del punteggio per i vari piloti
                pilots[l, k, i] = pilots[l, k, i-1] + results[l, k, i]

            for k in range(0, grid):  
                if k == wins[l, i-1]:
                    #registra quale pilota vince i gran premi, poi quale squadra
                    wins[l, i-1] = pilots[l, k, i]
                    if k < teams:
                        t_wins[l, i-1] = pilots[l, k, i] + pilots[l, k+teams, i]
                    elif k >= teams:
                        t_wins[l, i-1] = pilots[l, k, i] + pilots[l, k-teams, i]            

            # calcolo se un pilota ha matematicamente vinto la stagione
            #NB NON IL MASSIMO FARE SORT OGNI VOLTA, SI PUò FARE DIVERSAMENTE?
            provv_score = np.sort(pilots[l, :, i])
            if champ_check[l, 1] == 0 and (provv_score[grid-1]-provv_score[grid-2]) > 25*(races-1-i):
                #salva la posizione dove scrivere etichetta 'Champ.'
                champ_check[l] = [i, provv_score[grid-1]]
                
                
        # posizione nel grafico annuale piloti dei marker dei dnf
        for p in range(dnf_count[l]):
            dnf_markers[l][p] = [provv_dnf[l, p, 0], pilots[l, provv_dnf[l, p, 1], provv_dnf[l, p, 0]] ]
        
        
        for j in range(grid):
                # punti a fine stagione
                # servono per fig. più sotto, comodo prepararlo qui
                champ_p[l, j] = pilots[l, j, races-1]

#     print(dnf_count, np.mean(dnf_count))
    #---------------------------------------------------------------------------------------------            
    # grafici  

    yearplots_titles = tuple(yearplots_titles)

    # fig2 = make_subplots(rows=2, cols=1, vertical_spacing = 0.05, 
    #     subplot_titles=('WDC Championships every year', 'WCC Championships every year')
    #                     )
    
    fig2 = make_subplots(rows=2, cols=1, vertical_spacing = 0.05)

    for j in range(grid):
        fig2.add_trace(go.Scatter(x=np.linspace(1, years, years), y=champ_p[:, j],
                                     mode="lines+markers", line=dict(width=1.5, color=colors[j]),
                                     marker=dict(size=4), marker_symbol='x', name = names[j]),
                        row=1, col=1
                       )
    for j in range(teams):
        fig2.add_trace(go.Scatter(x=np.linspace(1, years, years), y=champ_p[:, j]+champ_p[:, j+teams],
                                     mode="lines+markers", line=dict(width=1.5, color=constr_colors[j]),
                                     marker=dict(size=4), marker_symbol='x', name = constr_names[j]),
                       row=2, col=1
                       )


    fig2.update_xaxes(tickvals=np.linspace(1, years, years), range=[0.9, years+0.1])
    fig2.update_layout(hovermode='x', height=1400, margin=dict(l=0, r=0, t=30, b=0),
                        showlegend=False)

    fig2.show()
    


    
    #grafici annuali campionato
    fig1 = make_subplots(rows=years, cols=2, 
        horizontal_spacing = 0.03, vertical_spacing = 0.004,
        subplot_titles=yearplots_titles)

    for l in range(0, years):
                
        for j in range(0, grid):

            # tracce dei piloti in una stagione
            fig1.add_trace(go.Scatter(x=np.linspace(0, races-1, races), 
                                    y=pilots[l, j, :], mode="lines+markers", 
                                    line=dict(width=1.5, color=colors[j]), 
                                    marker=dict(size=4), marker_symbol='x', name = names[j],
                                    customdata=np.concatenate(([0], pilots[l, j, 1:]- pilots[l, j, :(races-1)])),
                                    hovertemplate='%{y} , %{customdata}'),
                           row=l+1, col=1
                          )
        for j in range(0, teams):
            # tracce delle squadre in una stagione
            fig1.add_trace(go.Scatter(x=np.linspace(0, races-1, races), 
                                    y=pilots[l, j, :]+pilots[l, j+teams, :],
                                    mode="lines+markers", line=dict(width=1.5, color=constr_colors[j]), 
                                    marker=dict(size=4), marker_symbol='x', name = constr_names[j]),
                           row=l+1, col=2
                          )
            
        # icone dei dnf
        fig1.add_trace(go.Scatter(x=dnf_markers[l][:, 0], y=dnf_markers[l][:, 1],
                                  mode='markers', marker_symbol="x", marker_size=6, 
                                  marker_line_width=1.2, marker_line_color="white",
                                  marker_color="red", hoverinfo='skip',
                                  showlegend=False),
                        row=l+1, col=1
                       )

        # icone delle vittorie
        fig1.add_trace(go.Scatter(x=np.linspace(1, races, races), y=wins[l],
                                  mode='text', text=['W']*races, showlegend=False,
                                  textposition="middle center", hoverinfo='skip'),
                        row=l+1, col=1
                       )
        fig1.add_trace(go.Scatter(x=np.linspace(1, races, races), y=t_wins[l],
                                  mode='text', text=['W']*races, showlegend=False,
                                  textposition="middle center", hoverinfo='skip'),
                        row=l+1, col=2
                       )

# maniera alternativa di segnalare le vittorie
#         fig1.add_trace(go.Scatter(x=np.linspace(1, races, races), y=wins[l],
#                                   mode='markers', marker_symbol="star", marker_size=8, marker_line_width=1,
#                                   marker_line_color="black", marker_color="lightskyblue", 
#                                   showlegend=False),
#                         row=l+1, col=1
#                        )
#         fig1.add_trace(go.Scatter(x=np.linspace(1, races, races), y=t_wins[l],
#                                   mode='markers', marker_symbol="star", marker_size=8, marker_line_width=1,
#                                   marker_line_color="black", marker_color="lightskyblue", 
#                                   showlegend=False),
#                         row=l+1, col=2
#                        )

        # quando il titolo viene vinto
        fig1.add_trace(go.Scatter(x=[champ_check[l, 0]], y=[champ_check[l, 1]],
                              mode='text', text=['Champ.']*races, showlegend=False,
                              textposition="top center", hoverinfo='skip'),
                        row=l+1, col=1
                       )
    #     fig1.add_trace(go.Scatter(x=[champ_check[0]], y=[champ_check[1]],
    #                           mode='text', text=['Champ.']*races, showlegend=False,
    #                           textposition="top center"),
    #                     row=1, col=2
    #                    )

    fig1.update_layout(showlegend=False, hovermode='x')

    fig1.update_xaxes(tickvals=np.linspace(1, races-1, races-1), range=[0, races-0.8])

    fig1.update_layout(height=years*700, margin=dict(l=0, r=0, t=20, b=00))

    fig1.show()


    end = time.time()
#     print('t = ', end-start)
    
#     breakpoint()
#------------------------------------------------------------------------------------------------------

main()