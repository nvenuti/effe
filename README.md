We're having a bit of fun with F1.

Main things:

`f1_racetrace.py` : script that uses the [Fastf1](https://github.com/theOehrly/Fast-F1) library to plot time trace for races. To visualize how the strategy is playing out across the race.

`f1_plotly.py` : script that generates random results of an imaginary F1 grid along multiple seasons. Attemps to be as lifelike as possible.  
